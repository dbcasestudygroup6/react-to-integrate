import React from 'react';
import AverageGraph from './AverageGraph';
import Axios from 'axios';
/*
* This component will display boxes filled with TBD
*/
const average = (props) => {

  const data = {
    labels: ['Instrument1', 'Instrument2', 'Instrument3', 'Instrument4', 'Instrument5', 'Instrument6', 'Instrument7'],
    datasets: [
      {
        label: 'Average Sell',
        fill: true,
        lineTension: 0.1,
        backgroundColor: 'rgba(255,0,0,0.4)',
        borderColor: 'rgba(255,0,0,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(255,0,0,0.4)',
        pointHoverBorderColor: 'rgba(255,0,0,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [65, 59, 80, 81, 56, 55, 40]
      },
      {
          label: 'Average Buy',
          fill: true,
          lineTension: 0.1,
          backgroundColor: 'rgba(0,0,255,0.4)',
          borderColor: 'rgba(0,0,255,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(0,0,255,1)',
          pointHoverBorderColor: 'rgba(0,0,255,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [90, 84, 50, 41, 66, 75, 100]
        },
        {
          label: 'Current',
          fill: true,
          lineTension: 0.1,
          backgroundColor: 'rgba(0,255,0,0.4)',
          borderColor: 'rgba(0,255,0,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(0,255,0,1)',
          pointHoverBorderColor: 'rgba(0,255,0,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [90, 84, 50, 41, 66, 75, 100]
        }
    ]
  };


  return (
    <div className={props.className}>
      <AverageGraph data={data}/>
    </div>
  );
}

export default average;