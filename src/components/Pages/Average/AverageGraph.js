import React, {Component} from 'react';
import { Bar } from 'react-chartjs-2';



export default class AverageGraph extends Component {

  render() {
    return (
      <div>
        <h2>Average Graph</h2>
        <Bar ref="chart" data={this.props.data} />
      </div>
    );
  }

  componentDidMount() {
    const { datasets } = this.refs.chart.chartInstance.data
    console.log(datasets[0].data);
  }
}

