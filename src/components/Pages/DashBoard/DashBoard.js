import React from 'react';
import Grid from '../../../containers/Grid/Grid';

/*
* This component will display boxes filled with TBD
*/
const dashboard = (props) => {
  return (
    <div className='grid-container'>
      <Grid />
    </div>
  );
}

export default dashboard;