import React from 'react';
import Card from '../../Card/Card';
/*
* This component will display boxes filled with TBD
*/
const effective = (props) => {
  return (
    <div className={props.className}>
    <Card pageName={props.pageName} value={-5} />
  </div>
  );
}

export default effective;