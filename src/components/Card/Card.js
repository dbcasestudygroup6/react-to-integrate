import React from 'react';
/*
* This component will display boxes filled with TBD
*/
const card = (props) => {
  const style = {
    color: 'green'
  }
  if (props.value < 0 ) {
    style.color = 'red'
  }
  
  return (
    <div className='Card' style={style}>
      {props.pageName}
      <br/>
      <h1 className='numberCircle'>
      {props.value}
      </h1>
    </div>
  );
}

export default card;