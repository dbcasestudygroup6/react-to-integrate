import React from 'react';
import './Grid.css';
import Average from '../../components/Pages/Average/Average';
import Position from '../../components/Pages/Position/Position';
import Effective from '../../components/Pages/Effective/Effective';
import Realized from '../../components/Pages/Realized/Realized';
import Historical from '../../components/Pages/Historical/Historical';


const grid = (props) => {
    return (
      <div className='grid-container'>
        <Average pageName='Average' className='item1'  />      
        <Position pageName='Position' className='item2' />
        <Effective pageName='Effective' className='item3' />
        <Realized pageName='Realized' className='item4' />
        <Historical pageName='Historical' className='item5' />
      </div>
    );
  }
  
  export default grid;