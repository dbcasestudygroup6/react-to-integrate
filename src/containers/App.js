import React, { Component } from 'react';
import './App.css';
import NavBar from '../components/Navigation/NavBar';
import DashBoard from '../components/Pages/DashBoard/DashBoard';
import Average from '../components/Pages/Average/Average';
import Position from '../components/Pages/Position/Position';
import Effective from '../components/Pages/Effective/Effective';
import Realized from '../components/Pages/Realized/Realized';
import Historical from '../components/Pages/Historical/Historical';
import Footer from '../components/Footer/Footer'

class App extends Component {
  state = {
    //Could make a seperate data file of pages
    pages: [
          {name: 'DashBoard'},
          {name: 'Average'},
          {name: 'Position'},
          {name: 'Effective'},
          {name: 'Realized'},
          {name: 'Historical'}
    ],
    currentPage: 'DashBoard'
  }

  getCurrentPage = (newCurrent) => {
    this.setState({
      currentPage: newCurrent
    })
  }

  render() {

    let page = null;

    switch(this.state.currentPage) {
      case this.state.pages[0].name:
        page = (<DashBoard pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[1].name:
        page = (<Average pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[2].name:
        page = (<Position pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[3].name:
        page = (<Effective pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[4].name:
          page = (<Realized pageName={this.state.currentPage}/>);
          break;
      case this.state.pages[5].name:
          page = (<Historical pageName={this.state.currentPage}/>);
          break;
      default:
        page = (<DashBoard />);
    }
    return (
      <div className="App">
      <NavBar
        pages={this.state.pages}
        click={this.getCurrentPage}
        current={this.state.currentPage}/>
      {page}
      <Footer />
      </div>
    );
  }
}

export default App;